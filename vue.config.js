const path = require('path')
const TerserPlugin = require('terser-webpack-plugin')
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const webpack = require('webpack')

function resolve (dir) {
  return path.join(__dirname, dir)
}

module.exports = {
  configureWebpack: {
    plugins: [
      // new BundleAnalyzerPlugin(),
      new webpack.ProvidePlugin({
        'window.Quill': 'quill/dist/quill.js',
        'Quill': 'quill/dist/quill.js'
      }),
    ],
    // externals: {
    //   'Quill': 'Quill'
    // },
    optimization: {
      // splitChunks: {
      //   cacheGroups: {
      //     commons: {
      //       test: /[\\/]node_modules[\\/]quill/,
      //       name: 'vendors',
      //       chunks: 'all'
      //     }
      //   }
      // },
      // minimize: true,
      minimizer: [
        new TerserPlugin({
          // sourceMap: true,
          terserOptions: {
            compress: {
              drop_console: true,
            },
          },
        })
      ]
    }
  },
  productionSourceMap: false,
  // devServer: {
  //   proxy: {
  //     '/api': {
  //       target: 'http://192.168.1.204:80',
  //       pathRewrite: { '^/api': '' }
  //     }
  //   }
  // },
  chainWebpack: config => {
    config.resolve.alias
      .set('@', resolve('src'))
      .set('api', resolve('src/api'))
      .set('common', resolve('src/common'))
      .set('components', resolve('src/components'))
      .set('assets', resolve('src/assets'))
      .set('views', resolve('src/views'))
  },
  
  pluginOptions: {
    'style-resources-loader': {
      preProcessor: 'scss',
      patterns: [
        path.resolve(__dirname, './src/common/styles/variable.scss'),
        path.resolve(__dirname, './src/common/styles/mixin.scss')
      ]
    }
  }
}
