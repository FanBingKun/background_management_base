//说明：
//  1.如果加密解密涉及到前端和后端，则这里的key要保持和后端的key一致
//  2.AES的算法模式有好几种（ECB,CBC,CFB,OFB），所以也要和后端保持一致
//  3.AES的补码方式有两种（PKS5，PKS7），所以也要和后端保持一致
//  4.AES的密钥长度有三种（128,192,256，默认是128），所以也要和后端保持一致
//  5.AES的加密结果编码方式有两种（base64和十六进制），具体怎么选择由自己定，但是加密和解密的编码方式要统一

const encUtf8 = require('crypto-js/enc-utf8');
const encHex = require('crypto-js/enc-hex');
const encBase64 = require('crypto-js/enc-base64');
const AES = require('crypto-js/aes');
const modeEcb = require('crypto-js/mode-ecb');
const Pkcs7 = require('crypto-js/pad-pkcs7');

const key = '8888888888888888'
const keyArr = encUtf8.parse(key);

// 加密
function aesEncode(str) {
  const encryptedData = AES.encrypt(str, keyArr, {
    mode: modeEcb,
    padding: Pkcs7,
  });
  return encryptedData.ciphertext.toString();
}

// 解密
function aesDecode(encryptedStr) {
  var encryptedHexStr  = encHex.parse(encryptedStr);
  var encryptedBase64Str  = encBase64.stringify(encryptedHexStr);
  var decryptedData  = AES.decrypt(encryptedBase64Str, key, {
      mode: modeEcb,
      padding: Pkcs7
  });
  return decryptedData.toString(encUtf8);
}

export {
  aesEncode,
  aesDecode
}