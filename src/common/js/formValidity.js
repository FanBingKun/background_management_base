// 浮点数
let floatNumReg = /^\d+\.?\d+?$/

var checkNumber = (rule, value, callback) => {
  if (!floatNumReg.test(value)) {
    return callback(new Error('输入为数字'));
  }else{
    callback();
  }
};

export {
  checkNumber
}