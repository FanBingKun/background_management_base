export function removeTAG(str) {
  return str.replace(/<[^>]+>/g, "")
}

// 深拷贝 有个bug就是不能 属性不能放自己
export function deepClone(value) {
  if (value == null) return value
  if (typeof value !== 'object') return value
  if (value instanceof RegExp) return new RegExp(value)
  if (value instanceof Date) return new Date(value)
  let instance = new value.constructor
  for (let key in value) {
    if (value.hasOwnProperty(key)) {
      instance[key] = deepClone(value[key])
    }
  }
  return instance
}

// 数字 元 换成 分
export function yuantofen(value) {
  let str = ''+value
  let isNegative = false
  if(str.indexOf('-')>-1){
    str = str.slice(1)
    isNegative = true
  }
  if(str.length<3){
    str = '0.'+(('00'+str).slice(-2))
  }else{
    let end = str.slice(-2)
    let start = str.slice(0, -2)
    str = start+'.'+end
  }
  return isNegative ? '-'+str : str 
}

// 字符串转为时间搓
export function timestrToNum(str) {
  let num = new Date(str).setHours(0)
  return num
}

// 获取一个月有多少tian
export function getMonthDayNum(str) {
  var date = new Date(str);
  var year = date.getFullYear();
  var month = date.getMonth()+1;
  var d = new Date(year, month, 0);
  return d.getDate();
}

// 获取1号是周几
export function getMonth1haoDay(str) {
  let num = new Date(str).getDay()
  if(num == 0){
    num = 7
  }
  return num
}