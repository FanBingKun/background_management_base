import { postWithLoading } from 'api/axios'

export {
  getSysEvaluateList,
  deleteEvaluate,
  updateEvaluateShow
}

// 获取评价列表
function getSysEvaluateList(data) {
  return postWithLoading('/tour/admin/evaluate/getSysEvaluateList', data)
}

// 删除评价 
function deleteEvaluate(data) {
  return postWithLoading('/tour/admin/evaluate/deleteEvaluate', data)
}

// 修改显示状态  is_show":"1显示 0隐藏  
function updateEvaluateShow(data) {
  return postWithLoading('/tour/admin/evaluate/updateEvaluateShow', data)
}