import { postWithLoading } from 'api/axios'

export {
  addOrModifyLable,
  deleteLable,
  getLablePageAll,
  getLablePageList
}

// 添加获取编辑系列
function addOrModifyLable(data){
  if(data.lable_id){
    return postWithLoading('/tour/admin/syslable/modifyLable', data)
  }
  return postWithLoading('/tour/admin/syslable/addLable', data)
}

// 删除系列 
function deleteLable(id) {
  let data = {
    lable_id: id
  }
  return postWithLoading("/tour/admin/syslable/deleteLable", data);
}

// 获取全部列表
function getLablePageAll(data) {
  return postWithLoading("/tour/admin/syslable/getLablePageAll", data);
}

// 获取分页列表  
function getLablePageList(data) {
  return postWithLoading("/tour/admin/syslable/getLablePageList", data);
}