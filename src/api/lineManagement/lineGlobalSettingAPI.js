import { postWithLoading } from 'api/axios'

export {
  getInsuranceList,
  addOrModifyInsurance,
  deleteInsurance,
  editContract,
  editReserveInfo,
  getContract,
  getInsuranceAll,
  getReserveInfo,
  getPenaltyList,
  modifyOrAddPenalty,
  deletePenalty
}

// 删除保险 
function deleteInsurance(insurance_id) {
  let data = {
    insurance_id
  }
  return postWithLoading('/tour/admin/sysconfig/deleteInsurance', data)
}
// 添加获取编辑保险
function addOrModifyInsurance(data){
  if(data.insurance_id){
    return postWithLoading('/tour/admin/sysconfig/modifyInsurance', data)
  }
  return postWithLoading('/tour/admin/sysconfig/addInsurance', data)
}
// 获取保险列表全部
function getInsuranceAll(data) {
  return postWithLoading('/tour/admin/sysconfig/getInsuranceAll', data)
}
// 分页
function getInsuranceList(data) {
  return postWithLoading('/tour/admin/sysconfig/getInsuranceList', data)
}

// 合同条款
function editContract(data) {
  return postWithLoading('/tour/admin/sysconfig/editContract', data)
}
function getContract() {
  return postWithLoading('/tour/admin/sysconfig/getContract')
}

// 预订须知
function editReserveInfo(data) {
  return postWithLoading('/tour/admin/sysconfig/editReserveInfo', data)
}
function getReserveInfo() {
  return postWithLoading('/tour/admin/sysconfig/getReserveInfo')
}

// 退改规则
function getPenaltyList(data) {
  return postWithLoading('/tour/admin/sysconfig/getPenaltyList', data)
}
function modifyOrAddPenalty(data) {
  if(data.penalty_id) {
    return postWithLoading('/tour/admin/sysconfig/modifyPenalty', data)
  }else{
    return postWithLoading('/tour/admin/sysconfig/addPenalty', data)
  }
}
function deletePenalty(id){
  return postWithLoading('/admin/sysconfig/deletePenalty', {penalty_id: id})
}