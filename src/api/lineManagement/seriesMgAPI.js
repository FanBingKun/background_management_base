import { postWithLoading } from 'api/axios'

export {
  addOrModifySeries,
  deleteSeries,
  getSeriesAll,
  getSeriesPageList,
  rankSeries
}

// 添加获取编辑系列
function addOrModifySeries(data){
  if(data.series_id){
    return postWithLoading('/tour/admin/sysseries/modifySeries', data)
  }
  return postWithLoading('/tour/admin/sysseries/addSeries', data)
}

// 删除系列 
function deleteSeries(id) {
  let data = {
    series_id: id
  }
  return postWithLoading("/tour/admin/sysseries/deleteSeries", data);
}

// 获取全部列表
function getSeriesAll(data) {
  return postWithLoading("/tour/admin/sysseries/getSeriesAll", data);
}

// 获取分页列表  
function getSeriesPageList(data) {
  return postWithLoading("/tour/admin/sysseries/getSeriesPageList", data);
}

// 排序
function rankSeries(data) {
  return postWithLoading("/tour/admin/sysseries/rankSeries", data);
}