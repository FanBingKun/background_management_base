import { postWithLoading, post } from 'api/axios'

export {
  auditCommodity,
  getCommodityList,
  serachArea,
  soldOutCommodity,
  getCommodityDetail,
  addOrModifyCommodity,
  getAuditList,
  rankCommodity,
  deleteCommodity,
  getDistrictList,
  getAuditCommodityList
}
// 添加或者编辑
function addOrModifyCommodity(data){
  if(data.commodity_id) {
    return modifyCommodity(data)
  }else{
    return addCommodity(data)
  }
}
// 获取地址信息
function getDistrictList(data) {
  return postWithLoading('/tour/pub/district/getDistrictList', data)
}
// 添加商品
function addCommodity(data) {
  return postWithLoading('/tour/admin/commodity/addCommodity', data)
}
// 编辑商品
function modifyCommodity(data) {
  return postWithLoading('/tour/admin/commodity/modifyCommodity', data)
}
// 删除线路
function deleteCommodity(data) {
  return postWithLoading('/tour/admin/commodity/deleteCommodity', data)
}
// 线路审核
function auditCommodity(data) {
  return postWithLoading('/tour/admin/commodity/auditCommodity', data)
}
// 获取线路列表
function getCommodityList(data) {
  return postWithLoading('/tour/admin/commodity/getCommodityList', data)
}
// 获取审核线路列表
function getAuditCommodityList(data) {
  return postWithLoading('/tour/admin/commodity/getAuditCommodityList', data)
}
// 搜索出发地
function serachArea(value) {
  let data = {
    query_value: value
  }
  return post('/tour/admin/commodity/serachArea', data)
}

// 下架线路   变成申请上下架
function soldOutCommodity(data) {
  return postWithLoading('/tour/admin/commodity/applyCommodityRelease', data)
}

// 获取线路详情
function getCommodityDetail(data) {
  return postWithLoading('/tour/admin/commodity/getCommodityDetail', data)
}

// 查看审核详情
function getAuditList(data) {
  return postWithLoading('/tour/admin/commodity/getAuditList', data)
}

// 审核排序
function rankCommodity(data) {
  return postWithLoading('/tour/admin/commodity/rankCommodity', data)
}