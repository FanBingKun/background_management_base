import { catchError } from 'api/axios'

// 获取分类列表
export function getCateGoryList(page, data={}){
  data.pageInfo ={
    page: page,
    limit: 10
  }
  return catchError('/admin/syscategory/getCateGoryList', data)
}

// 获取全部分类列表
export function getCateGoryAllList(data={}){
  return catchError('/admin/syscategory/getAllCateGoryList', data)
}

// 添加获取编辑
export function addOrEditCateGory(data){
  if(data.category_id){
    return catchError('/admin/syscategory/modifyCateGory', data)
  }else{
    return catchError('/admin/syscategory/addCateGory', data)
  }
}

// 删除分类
export function delCateGory(id){
  let data = {
    category_id: id
  }
  return catchError('/admin/syscategory/deleteCateGory', data)
}

// 排序分类
export function rankCateGory(data){
  return catchError('/admin/syscategory/rankCateGory', data)
}