import { postWithLoading } from 'api/axios'

// 获取管理员列表
export function getSysUserList(data){
  return postWithLoading('/tour/admin/sysuser/getSysUserList', data)
}

// 添加 或者 编辑 管理员
export function addOrEditAdm(data){
  if(data.user_id){
    return postWithLoading('/tour/admin/sysuser/modifySysUser', data)
  }else{
    return postWithLoading('/tour/admin/sysuser/addSysUser', data)
  }
}

// 删除管理员
export function delAdm(id){
  let data = {
    user_id: id
  }
  return postWithLoading('/tour/admin/sysuser/deleteUser', data)
}

// 用户 登陆
export function adminLogin(data){
  return postWithLoading('/tour/admin/sysuser/login', data)
}