import { get, catchError } from "api/axios";
import * as qiniu from "qiniu-js";

const menuArr = require('./menu').default

// 获取菜单 api
export function getMenuList() {
  return menuArr;
}

// 获取该管理员底下的菜单
export function getAdminMenuList() {
  return menuArr;
}

export function getMenuListApi() {
  return catchError("/admin/sysrole/getMenuList");
}

// 获取七牛云token
export function getQiniuToken() {
  return get("/tour/pub/qiniu/getToken");
}

// 上传图片
export async function commonUpload(file) {
  try {
    let res = await getQiniuToken();
    let token = res.rData[0].token;
    let res2 = await _upImg(file, file.name, token);
    return res2;
  } catch (error) {
    console.log(error);
    return null;
  }
}

// 获取地区信息


function _upImg(file, key, token) {
  return new Promise((resolve, reject) => {
    let putExtra = {
      fname: "",
      params: {},
      mimeType: null
    };
    let config = {
      useCdnDomain: true,
      region: null
    };
    var observable = qiniu.upload(file, key, token, putExtra, config);
    observable.subscribe({
      next: res => {
        console.log("上传中", res);
      },
      error: err => {
        reject(err);
      },
      complete: res => {
        console.log('11', res);
        resolve(res.key);
      }
    });
  });
}
