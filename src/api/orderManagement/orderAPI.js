import { postWithLoading } from 'api/axios';

let urlMap = {
  detail: '/admin/commodityOrder/getOrderDetailInfo',
  get: '/admin/commodityOrder/getCommodityOrderList',
  del: '/admin/commodityOrder/deleteOrder',
  cancel: '/admin/commodityOrder/cancelOrder',
  confirm: '/admin/commodityOrder/affirmOrder'
}

function orderAPI(type, data = {}) {
  let url = '/tour'
  return postWithLoading(
    url+urlMap[type],
    data
  );
}

export default orderAPI