import { catchError } from 'api/axios'

// 获取内容列表
export function getList(page, data={}){
  data.pageInfo ={
    page: page,
    limit: 10
  }
  return catchError('/admin/sysMessage/getMessageList', data)
}

// 添加
export function add(data){
  return catchError('/admin/sysMessage/addMessage', data)
}

// 删除 
export function del(id){
  let data = {
    msg_id: id
  }
  return catchError('/admin/sysMessage/deleteMessage', data)
}