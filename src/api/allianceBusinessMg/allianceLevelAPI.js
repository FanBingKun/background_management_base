import { postWithLoading } from 'api/axios';

let urlMap = {
  list: '/admin/alliance/getAllianceGradeAll', // 获取加盟商等级
  add: '/admin/alliance/addAllianceGrade', // 删除加盟商
  edit: '/admin/alliance/modifyAllianceGrade', // 编辑加盟商等级
  del: '/admin/alliance/deleteAllianceGrade', // 删除加盟商列表
}

function allianceLevelAPI(type, data = {}) {
  let url = '/tour'
  return postWithLoading(
    url+urlMap[type],
    data
  );
}

export default allianceLevelAPI