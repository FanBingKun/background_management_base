import { postWithLoading } from 'api/axios';

let urlMap = {
  add: '/admin/alliance/addAlliance', // 添加加盟商基本信息
  del: '/admin/alliance/deleteAlliance', // 删除加盟商
  get: '/admin/alliance/getAllianceList', // 获取加盟商列表
  getAccountInfo: '/admin/alliance/getAllianceAccountInfo', // 获取加盟商账号信息
  detail: '/admin/alliance/getAllianceDetail', // 获取加盟商详情
  getMenu: '/admin/alliance/getMenuList', // 获取可编辑菜单列表
  editAccountInfo: '/admin/alliance/modifyAccountInfo',// 编辑账号信息
  edit: '/admin/alliance/modifyAlliance', // 编辑加盟商基本信息
  editMenu: '/admin/alliance/modifyAllianceMenu' // 编辑菜单
}

function allianceListAPI(type, data = {}) {
  let url = '/tour'
  return postWithLoading(
    url+urlMap[type],
    data
  );
}

export default allianceListAPI