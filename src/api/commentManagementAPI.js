import { catchError } from 'api/axios'

// 获取评论列表
export function getList(page, data={}){
  data.pageInfo ={
    page: page,
    limit: 10
  }
  return catchError('/admin/comment/getSysCommentList', data)
}


// 删除
export function del(data={}){
  return catchError('/admin/comment/deleteComment', data)
}

// 获取详情评论
export function getDetailCommentList(page, data){
  data.pageInfo ={
    page: page,
    limit: 10
  }
  return catchError('/admin/comment/getDetailCommentList', data)
}

// 获取子评论
export function getDetailHFCommentList(page, data){
  data.pageInfo ={
    page: page,
    limit: 5
  }
  return catchError('/admin/comment/getDetailHFCommentList', data)
}