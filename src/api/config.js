const debug = process.env.NODE_ENV !== 'production'
let config = {
}
if (debug) {
  config.serverUrl = 'https://tourplatformapi.tianrunhome.com:8801'
  config.imgUrl = 'https://trfile.tianrunhome.com/'
} else {
  config.serverUrl = 'https://tourplatformapi.tianrunhome.com:8801'
  config.imgUrl = 'https://trfile.tianrunhome.com/'
}


export default config
