import { postWithLoading } from 'api/axios';

let urlMap = {
  get: '/admin/memberUser/getMemberUserRecordList'
}

function memberRecordAPI(type, data = {}) {
  let url = '/tour'
  return postWithLoading(
    url+urlMap[type],
    data
  );
}

export default memberRecordAPI