import { postWithLoading } from 'api/axios';

let urlMap = {
  addMemberBenefit: '/admin/memberConfig/addMemberBenefit', // 添加权益
  addMemberConfigBase: '/admin/memberConfig/addMemberConfigBase', // 添加基础信息
  addMemberCoupon: '/admin/memberConfig/addMemberCoupon', // 添加会员套餐优惠券

  deleteMemberBenefit: '/admin/memberConfig/deleteMemberBenefit', // 删除权益说明
  deleteMemberCoupon: '/admin/memberConfig/deleteMemberCoupon', // 删除会员套餐优惠券
  deleteMemberConfig: '/admin/memberConfig/deleteMemberConfig', // 删除会员套餐列表  不确定

  getMemberBenefitList: '/admin/memberConfig/getMemberBenefitList', // 获取权益说明列表
  getMemberCouponList: '/admin/memberConfig/getMemberCouponList', // 获取优惠券配置列表
  getMemberConfigAllList: '/admin/memberConfig/getMemberConfigAllList', // 获取会员套餐列表

  modifyMemberBenefit: '/admin/memberConfig/modifyMemberBenefit', // 编辑权益说明
  modifyMemberConfigBase: '/admin/memberConfig/modifyMemberConfigBase', // 编辑会员套餐基本信息
  modifyMemberCoupon: '/admin/memberConfig/modifyMemberCoupon', // 编辑会员套餐优惠券

  getMemberAgreement: '/admin/memberConfig/getMemberAgreement', // 获取会员协议
  modifyMemberAgreement: '/admin/memberConfig/modifyMemberAgreement', // 编辑会员协议
}

function memberComboAPI(type, data = {}) {
  let url = '/tour'
  return postWithLoading(
    url+urlMap[type],
    data
  );
}

export default memberComboAPI