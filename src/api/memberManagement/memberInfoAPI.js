import { postWithLoading } from 'api/axios';

let urlMap = {
  get: '/admin/memberUser/getMemberUserList',
  search: '/admin/memberUser/searchMemberUserList', // 搜索会员列表
  add: '/admin/memberUser/addMemberUser',
  edit: '/admin/memberUser/modifyMemberUser'
}

function memberInfoAPI(type, data = {}) {
  let url = '/tour'
  return postWithLoading(
    url+urlMap[type],
    data
  );
}


export default memberInfoAPI