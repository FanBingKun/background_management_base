import { postWithLoading } from 'api/axios'

// 获取位置列表
export function getPositionList(){
  return postWithLoading('/tour/admin/sysad/getAdWhichPageList')
}

// 获取轮播列表
export function getADList(data){
  return postWithLoading('/tour/admin/sysad/getAdList', data)
}

// 改变状态
export function updateStatus(data){
  return postWithLoading('/tour/admin/sysad/updateADStatus', data)
}

// 改变排序
export function rank(data) {
  return postWithLoading('/tour/admin/sysad/rankAD', data)
}

// 添加或者编辑广告
export function addOrEditAd(data){
  if(data.ad_id){
    return postWithLoading('/tour/admin/sysad/modifyAD', data)
  }else{
    return postWithLoading('/tour/admin/sysad/addAD', data)
  }
}

// 删除广告
export function delAd(ad_id){
  let data = {
    ad_id
  }
  return postWithLoading('/tour/admin/sysad/deleteAd', data)
}

// 获取启动页视频
export function getStartAd() {
  return postWithLoading('/tour/admin/sysad/getStartAd')
}

// 编辑启动页视频
export function updateStartAd(data) {
  return postWithLoading('/tour/admin/sysad/modifyStartAd', data)
}