import { catchError } from 'api/axios'

// 获取用户列表
export function getUserList(page, data={}){
  data.pageInfo ={
    page: page,
    limit: 10
  }
  return catchError('/admin/appuser/getUserList', data)
}

// 修改黑名单状态
export function updateHMD(data){
  return catchError('/admin/appuser/updateHMD', data)
}

// 修改禁止状态
export function updateUserStatus(data){
  return catchError('/admin/appuser/updateUserStatus', data)
}

// 获取详情
export function getDetailUserInfo(data) {
  return catchError('/admin/appuser/getDetailUserInfo', data)
}