import { catchError } from 'api/axios'

// 获取内容列表
export function getDynamicList(page, data={}){
  data.pageInfo ={
    page: page,
    limit: 10
  }
  return catchError('/admin/sysDynamic/getSysDynamicList', data)
}

// 获取详情
export function getDetail(data={}){
  return catchError('/admin/sysDynamic/getDynamicDetail', data)
}

// 删除内容
export function del(data={}){
  if(data.info_ids){
    return catchError('/admin/sysDynamic/deleteBatchDynamic', data)
  }else{
    return catchError('/admin/sysDynamic/deleteDynamic', data)
  }
}
