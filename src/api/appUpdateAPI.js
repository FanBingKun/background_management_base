import { catchError } from 'api/axios'

// 获取内容列表
export function getAppVersionList(page, data={}){
  data.pageInfo ={
    page: page,
    limit: 10
  }
  return catchError('/admin/sysappversion/getAppVersionList', data)
}

// 添加 或者 编辑
export function addOrEditAppversion(data){
  if(data.v_id){
    return catchError('/admin/sysappversion/modifyVersion', data)
  }else{
    return catchError('/admin/sysappversion/addVersion', data)
  }
}

// 删除 
export function del(id){
  let data = {
    v_id: id
  }
  return catchError('/admin/sysappversion/deleteVersion', data)
}