import { postWithLoading } from 'api/axios';

export {
  addOrModifyGroupPurchase,
  getGroupPurchaseList,
  getGroupPurchaseDetail,
  deleteGroupPurchase,
  updateGroupRelease,
  getAuditList,
  auditGroupPurchase
};
// 添加或者编辑
function addOrModifyGroupPurchase(data) {
  if (data.gp_id) {
    return modifyGroupPurchase(data);
  } else {
    return addGroupPurchase(data);
  }
}

// 添加
function addGroupPurchase(data) {
  return postWithLoading('/tour/admin/groupPurchase/addGroupPurchase', data);
}
// 编辑
function modifyGroupPurchase(data) {
  return postWithLoading('/tour/admin/groupPurchase/modifyGroupPurchase', data);
}
// 获取列表
function getGroupPurchaseList(data) {
  return postWithLoading(
    '/tour/admin/groupPurchase/getGroupPurchaseList',
    data
  );
}
// 获取详情
function getGroupPurchaseDetail(id) {
  return postWithLoading('/tour/admin/groupPurchase/getGroupPurchaseDetail', {
    gp_id: id
  });
}
// 删除拼团
function deleteGroupPurchase(id) {
  return postWithLoading('/tour/admin/groupPurchase/deleteGroupPurchase', {
    gp_id: id
  });
}

// 下架线路 
function updateGroupRelease(data) {
  return postWithLoading('/tour/admin/groupPurchase/updateGroupRelease', data)
}
// 获取审核详情 
function getAuditList(data) {
  return postWithLoading('/tour/admin/groupPurchase/getAuditList', data)
}
// 获取审核
function auditGroupPurchase(data) {
  return postWithLoading('/tour/admin/groupPurchase/auditGroupPurchase', data)
}
