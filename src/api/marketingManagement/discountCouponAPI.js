import { postWithLoading } from 'api/axios';

let urlMap = {
  add: '/admin/coupon/addCoupon',
  edit: '/admin/coupon/modifyCoupon',
  del: '/admin/coupon/deleteCoupon',
  get: '/admin/coupon/getCouponList'
}
 
function discountCouponAPI(type, data = {}) {
  let url = '/tour'
  return postWithLoading(
    url+urlMap[type],
    data
  );
}

export default discountCouponAPI