// 获取供应商列表
import { postWithLoading } from 'api/axios'

export {
  getSupplierAll
}

// 添加商品
function getSupplierAll(data) {
  return postWithLoading('/tour/admin/supplier/getSupplierAll', data)
}