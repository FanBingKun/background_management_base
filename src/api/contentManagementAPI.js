import { catchError } from 'api/axios'

// 获取内容列表
export function getContentList(page, data={}){
  data.pageInfo ={
    page: page,
    limit: 10
  }
  return catchError('/admin/sysinfo/getInfoList', data)
}
// 获取详情
export function getContentDetail(data={}){
  return catchError('/admin/sysinfo/getInfoDetail', data)
}

// 审核 
export function auditContent(data={}){
  return catchError('/admin/sysinfo/auditInfo', data)
}

// 改变显示状态 
export function updateContentStatus(data={}){
  return catchError('/admin/sysinfo/updateInfoStatus', data)
}

// 删除内容
export function delContent(data={}){
  if(data.info_ids){
    return catchError('/admin/sysinfo/deleteBatchInfo', data)
  }else{
    return catchError('/admin/sysinfo/deleteInfo', data)
  }
}