import { postWithLoading } from 'api/axios'

// 获取角色列表
export function getRolerList(page, data={}){
  return postWithLoading('/tour/admin/sysrole/getRoleList', data)
}

// 添加 或者 编辑 管理员
export function addOrEditRole(data){
  if(data.role_id){
    return postWithLoading('/tour/admin/sysrole/modifyRole', data)
  }else{
    return postWithLoading('/tour/admin/sysrole/addRole', data)
  }
}

// 删除管理员
export function delRole(id){
  let data = {
    role_id: id
  }
  return postWithLoading('/tour/admin/sysrole/deleteRole', data)
}

// 获取详情
export function getRoleInfo(id){
  let data = {
    role_id: id
  }
  return postWithLoading('/tour/admin/sysrole/getRoleInfo', data)
}


// 获取菜单列表
export function getMenuList(){
  return postWithLoading('/tour/admin/sysrole/getMenuList')
}