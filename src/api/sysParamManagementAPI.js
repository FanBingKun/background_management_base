import { catchError } from 'api/axios'

// 获取
export function getInfo(id){
  let data = {
    infosys_id: id
  }
  return catchError('/admin/sysParameter/getSyParameterInfo', data)
}

// {"infosys_id":1 注意 1免责声明 2信息公告 3客服微信 4 商务合作 }
// 添加
export function addInfo(data){
  return catchError('/admin/sysParameter/modifySyParameterInfo', data)
}