const arr = [
  // 系统管理
  {
    menu_id: 100,
    up_menu_id: 0,
    menuZHName: "系统管理",
    menuENName: "sysManagement"
  },
  {
    menu_id: 6,
    up_menu_id: 100,
    menuZHName: "广告管理",
    menuENName: "advertisingManagement"
  },
  {
    menu_id: 15,
    up_menu_id: 6,
    menuZHName: "轮播图",
    menuENName: "carouselManagement"
  },
  {
    menu_id: 13,
    up_menu_id: 100,
    menuZHName: "管理员管理",
    menuENName: "administratorManagement"
  },
  {
    menu_id: 14,
    up_menu_id: 100,
    menuZHName: "角色权限管理",
    menuENName: "roleAuthorityManagement"
  },
  // 线路管理
  {
    menu_id: 101,
    up_menu_id: 0,
    menuZHName: "线路管理",
    menuENName: "lineManagement"
  },
  {
    menu_id: 201,
    up_menu_id: 101,
    menuZHName: "系列管理",
    menuENName: "seriesMg"
  },
  {
    menu_id: 202,
    up_menu_id: 101,
    menuZHName: "标签管理",
    menuENName: "tagMg"
  },
  {
    menu_id: 203,
    up_menu_id: 101,
    menuZHName: "线路全局设置",
    menuENName: "lineGlobalSetting"
  },
  {
    menu_id: 204,
    up_menu_id: 101,
    menuZHName: "线路商品",
    menuENName: "lineCommodity"
  },
  {
    menu_id: 205,
    up_menu_id: 101,
    menuZHName: "线路审核",
    menuENName: "lineReview"
  },
  {
    menu_id: 206,
    up_menu_id: 101,
    menuZHName: "线路评价",
    menuENName: "lineEvaluate"
  },
  // 营销管理
  {
    menu_id: 102,
    up_menu_id: 0,
    menuZHName: "营销管理",
    menuENName: "marketingManagement"
  },
  {
    menu_id: 103,
    up_menu_id: 0,
    menuZHName: "会员管理",
    menuENName: "memberManagement"
  },
  {
    menu_id: 206,
    up_menu_id: 103,
    menuZHName: "会员套餐",
    menuENName: "memberComboList"
  },
  {
    menu_id: 207,
    up_menu_id: 103,
    menuZHName: "会员信息",
    menuENName: "memberInfoList"
  },
  {
    menu_id: 208,
    up_menu_id: 103,
    menuZHName: "开通记录",
    menuENName: "openRecordList"
  },
  {
    menu_id: 209,
    up_menu_id: 103,
    menuZHName: "会员协议",
    menuENName: "memberAgreementList"
  },
  {
    menu_id: 301,
    up_menu_id: 102,
    menuZHName: "拼购",
    menuENName: "groupBuying"
  },
  {
    menu_id: 401,
    up_menu_id: 301,
    menuZHName: "拼购线路",
    menuENName: "groupBuyingList"
  },
  {
    menu_id: 402,
    up_menu_id: 301,
    menuZHName: "拼购审核",
    menuENName: "groupBuyingAudit"
  },
  {
    menu_id: 302,
    up_menu_id: 102,
    menuZHName: "优惠券",
    menuENName: "discountCouponList"
  },
  // 订单管理
  {
    menu_id: 601,
    up_menu_id: 0,
    menuZHName: "订单管理",
    menuENName: "orderManagement"
  },
  {
    menu_id: 4011,
    up_menu_id: 601,
    menuZHName: "订单列表",
    menuENName: "orderList"
  },
  // 加盟商管理
  {
    menu_id: 501,
    up_menu_id: 0,
    menuZHName: "加盟商管理",
    menuENName: "allianceBusinessMg"
  },
  {
    menu_id: 5011,
    up_menu_id: 501,
    menuZHName: "加盟商列表",
    menuENName: "allianceList"
  },
  {
    menu_id: 5012,
    up_menu_id: 501,
    menuZHName: "加盟商等级",
    menuENName: "allianceLevelList"
  }
];

export default arr