export default {
  setDiscountCouponObj (state, data) {
    state.discountCouponObj = data
  },
  setMemberInfoObj (state, data) {
    state.memberInfoObj = data
  },
  setEvaluateObj (state, data) {
    state.evaluateObj = data
  },
}
