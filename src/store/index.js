import Vue from 'vue'
import Vuex from 'vuex'
import createLogger from 'vuex/dist/logger'

// module
import menu from './menu'
import admoduleData from './admoduleData'
import middleData from './middleData'
Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

const store = new Vuex.Store({
  modules: {
    menu,
    admoduleData,
    middleData
  },
  strict: debug,
  plugins: debug ? [createLogger()] : []
})
export default store