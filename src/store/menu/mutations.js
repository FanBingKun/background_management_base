export default {
  saveRoutes (state, data) {
    state.anthMenuList = data
    state.hasMenu = true
  },
  saveTreeRoutes (state, data) {
    state.hasMenu = true
    state.treeMenuList = data
  }
}
