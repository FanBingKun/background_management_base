import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import formatDate from 'common/js/date'
import {yuantofen} from 'common/js/util'
// ele ui
import './plugins/element.js'
// 引入css
import 'common/styles/common.scss'
import beforeEach from '@/router/beforeRouterEnter'
console.log(beforeEach)
Vue.config.productionTip = false

for(let key in beforeEach) {
  router.beforeEach(beforeEach[key])
}

// 过滤器
Vue.filter('formatDate', (value, fmt) => {
  return formatDate(value*1000, fmt)
})
Vue.filter('yuanToFen', (value)=> {
  return yuantofen(value)
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

console.log('v1.1')