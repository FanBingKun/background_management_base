import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const Login = () => import(/* webpackChunkName: "login" */ 'views/login.vue')

const commonRoutes = [
  {
    path: '/',
    redirect: '/login'
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  }
]




const Routes = new Router({
  mode: 'history',
  routes: commonRoutes
})


export default Routes
