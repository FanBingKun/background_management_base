const NotFound = () => import(/* webpackChunkName: "404" */ 'views/404.vue')
const Test = () => import('views/test.vue')
const otherRoutes = [
  {
    path: '/notfound',
    name: 'notfound',
    component: NotFound
  },
  {
    path: '/test',
    name: 'test',
    component: Test
  },
  {
    path: '*',
    redirect: '/notfound'
  }
]

export default otherRoutes