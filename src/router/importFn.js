import SkeletonComponent from 'components/baseComponents/Skeleton'
import ErrorComponent from 'components/baseComponents/Error'
export default (fn) => {
  const component = () => ({
    // 需要加载的组件 (应该是一个 `Promise` 对象)
    component: fn(),
    // 异步组件加载时使用的组件
    loading: SkeletonComponent,
    // 加载失败时使用的组件
    error: ErrorComponent,
    // 展示加载时组件的延时时间。默认值是 200 (毫秒)
    delay: 200
  })
  return {
    render(h) {
      return h(component)
    }
  }
}