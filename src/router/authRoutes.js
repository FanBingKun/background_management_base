import importFn from '@/router/importFn'

const Home = importFn(() => import(/* webpackChunkName: "home" */ 'views/home.vue'))

// const BusinessCooperation = () => import(/* webpackChunkName: "businessCooperation" */ 'views/sysParamManagement/businessCooperation.vue')
// const SysPlatformNotice = () => import(/* webpackChunkName: "sysPlatformNotice" */ 'views/sysParamManagement/sysPlatformNotice.vue')
// const SysPlatformStated = () => import(/* webpackChunkName: "sysPlatformStated" */ 'views/sysParamManagement/sysPlatformStated.vue')
// const SysPlatformWechat = () => import(/* webpackChunkName: "sysPlatformWechat" */ 'views/sysParamManagement/sysPlatformWechat.vue')
// // 安卓升级
// const AndroidVersion = () => import(/* webpackChunkName: "androidVersion" */ 'views/sysParamManagement/androidVersion.vue')
// // ios升级
// const IOSVersion = () => import(/* webpackChunkName: "IOSVersion" */ 'views/sysParamManagement/IOSVersion.vue')

// // 分类管理
// const CategoryManagement = () => import(/* webpackChunkName: "categoryManagement" */ 'views/categoryManagement/categoryManagement.vue')
// // 内容管理
// const ContentManagement = () => import(/* webpackChunkName: "contentManagement" */ 'views/contentManagement/contentManagement.vue')

// // 动态管理
// const DynamicManagement = () => import(/* webpackChunkName: "dynamicManagement" */ 'views/dynamicManagement/dynamicManagement.vue')
// // 评论管理
// const CommentManagement = () => import(/* webpackChunkName: "commentManagement" */ 'views/commentManagement/commentManagement.vue')

// // 用户管理
// const UsersManagement = () => import(/* webpackChunkName: "usersManagement" */ 'views/usersManagement/usersManagement.vue')
// // 启动页
// const StartPage = importFn(() => import(/* webpackChunkName: "startPage" */ 'views/sysManagement/ADmanagement/startPage.vue'))

// 角色权限管理
const RoleAuthorityManagement = importFn(() => import(/* webpackChunkName: "RoleAuthorityManagement" */ 'views/sysManagement/RoleAuthorityManagement/RoleAuthorityManagement.vue'))
// 管理员管理
const AdministratorManagement = importFn(() => import(/* webpackChunkName: "administratorManagement" */ 'views/sysManagement/administratorManagement/administratorManagement.vue'))
// 系统消息 
// const SysMsgManagement = importFn(() => import(/* webpackChunkName: "sysMsgManagement" */ 'views/sysManagement/sysMsgManagement/sysMsgManagement.vue'))
// 轮播图 
const CarouselManagement = importFn(() => import(/* webpackChunkName: "carouselManagement" */ 'views/sysManagement/ADmanagement/carouselManagement.vue'))
// 添加轮播图
const CreateAd = importFn(() => import(/* webpackChunkName: "createAd" */ 'views/sysManagement/ADmanagement/createAd.vue'))

// 线路管理模块
// 系列管理
const SeriesMg = importFn(() => import(/* webpackChunkName: "seriesMg" */ 'views/lineManagement/seriesMg.vue'))
// 标签管理
const TagMg = importFn(() => import(/* webpackChunkName: "tagMg" */ 'views/lineManagement/tagMg.vue'))
// 线路全局设置
const LineGlobalSetting = importFn(() => import(/* webpackChunkName: "LineGlobalSetting" */ 'views/lineManagement/lineGlobalSetting.vue'))
// 线路商品
const lineCommodity = importFn(() => import(/* webpackChunkName: "lineCommodity" */ 'views/lineManagement/lineCommodity.vue'))
// 线路商品添加页面
const lineCommodityAdd = importFn(() => import(/* webpackChunkName: "lineCommodityAdd" */ 'views/lineManagement/lineCommodityAdd.vue'))
// 线路审核
const lineReview = importFn(() => import(/* webpackChunkName: "lineReview" */ 'views/lineManagement/lineReview.vue'))
// 线路审核详情
const lineReviewDetail = importFn(() => import(/* webpackChunkName: "lineReviewDetail" */ 'views/lineManagement/lineReviewDetail.vue'))
// 线路评价
const lineEvaluate = importFn(() => import(/* webpackChunkName: "lineEvaluate" */ 'views/lineManagement/lineEvaluate.vue'))
// 查看线路评价
const lineEvaluateDetail = importFn(() => import(/* webpackChunkName: "lineEvaluateDetail" */ 'views/lineManagement/lineEvaluateDetail.vue'))
// 营销管理
// 拼购线路列表
const groupBuyingList = importFn(() => import(/* webpackChunkName: "groupBuyingList" */ 'views/marketingManagement/groupBuying/groupBuyingList.vue'))
// 拼购线路添加
const groupBuyingAdd = importFn(() => import(/* webpackChunkName: "groupBuyingAdd" */ 'views/marketingManagement/groupBuying/groupBuyingAdd.vue'))
// 拼购线路审核
const groupBuyingAudit = importFn(() => import(/* webpackChunkName: "groupBuyingAudit" */ 'views/marketingManagement/groupBuying/groupBuyingAudit.vue'))
// 优惠券
const discountCouponList = importFn(() => import(/* webpackChunkName: "discountCouponList" */ 'views/marketingManagement/discountCoupon/list.vue'))
// 优惠券添加
const discountCouponDetail = importFn(() => import(/* webpackChunkName: "discountCouponDetail" */ 'views/marketingManagement/discountCoupon/detail.vue'))
// 会员管理
// 会员套餐列表
const memberComboList = importFn(() => import(/* webpackChunkName: "memberComboList" */ 'views/memberManagement/memberCombo/list.vue'))
// 会员添加
const memberComboDetail = importFn(() => import(/* webpackChunkName: "memberComboDetail" */ 'views/memberManagement/memberCombo/detail.vue'))
// 会员信息
const memberInfoList = importFn(() => import(/* webpackChunkName: "memberInfoList" */ 'views/memberManagement/memberInfo/list.vue'))
const memberInfoDetail = importFn(() => import(/* webpackChunkName: "memberInfoDetail" */ 'views/memberManagement/memberInfo/detail.vue'))
// 开通记录
const openRecordList = importFn(() => import(/* webpackChunkName: "openRecordList" */ 'views/memberManagement/openRecord/list.vue'))
// 会员协议
const memberAgreementList = importFn(() => import(/* webpackChunkName: "openRecordList" */ 'views/memberManagement/memberAgreement/list.vue'))

// 订单管理
// 普通订单
const orderList = importFn(() => import(/* webpackChunkName: "orderList" */ 'views/orderManagement/orderList.vue'))
// 普通订单详情
const orderDetail = importFn(() => import(/* webpackChunkName: "orderDetail" */ 'views/orderManagement/orderDetail.vue'))

// 加盟商管理
// 加盟商列表
const allianceList = importFn(() => import(/* webpackChunkName: "allianceList" */ 'views/allianceBusinessMg/allianceInfo/allianceList.vue'))
// 加盟商详情
const allianceDetail = importFn(() => import(/* webpackChunkName: "allianceDetail" */ 'views/allianceBusinessMg/allianceInfo/allianceDetail.vue'))
// 加盟商账号信息
const alliancAccountInfo = importFn(() => import(/* webpackChunkName: "alliancAccountInfo" */ 'views/allianceBusinessMg/allianceInfo/alliancAccountInfo.vue'))
// 加盟商等级列表
const allianceLevelList = importFn(() => import(/* webpackChunkName: "allianceLevelList" */ 'views/allianceBusinessMg/allianceLevel/list.vue'))

export default [
  {
    path: '/home',
    name: 'home',
    auth: 'home',
    component: Home,
    children:[
      {
        zhName: '角色权限管理',
        path: 'roleAuthorityManagement',
        name: 'roleAuthorityManagement',
        auth: 'roleAuthorityManagement',
        component: RoleAuthorityManagement
      },
      {
        zhName: '管理员管理',
        path: 'administratorManagement',
        name: 'administratorManagement',
        auth: 'administratorManagement',
        component: AdministratorManagement
      },
      // {
      //   zhName: '系统消息管理',
      //   path: 'sysMsgManagement',
      //   name: 'sysMsgManagement',
      //   auth: 'sysMsgManagement',
      //   component: SysMsgManagement
      // },
      {
        zhName: '轮播图',
        path: 'carouselManagement',
        name: 'carouselManagement',
        auth: 'carouselManagement',
        component: CarouselManagement
      },
      {
        zhName: '添加轮播图',
        path: 'createAd/:id?',
        name: 'createAd',
        auth: 'carouselManagement',
        component: CreateAd
      },
      {
        zhName: '系列管理',
        path: 'seriesMg',
        name: 'seriesMg',
        auth: 'seriesMg',
        component: SeriesMg
      },
      {
        zhName: '标签管理',
        path: 'tagMg',
        name: 'tagMg',
        auth: 'tagMg',
        component: TagMg
      },
      {
        zhName: '线路全局设置',
        path: 'lineGlobalSetting',
        name: 'lineGlobalSetting',
        auth: 'lineGlobalSetting',
        component: LineGlobalSetting
      },
      {
        zhName: '线路商品',
        path: 'lineCommodity',
        name: 'lineCommodity',
        auth: 'lineCommodity',
        component: lineCommodity
      },
      {
        zhName: '线路详情',
        path: 'lineCommodityAdd',
        name: 'lineCommodityAdd',
        auth: 'lineCommodity',
        component: lineCommodityAdd
      },
      {
        zhName: '线路审核',
        path: 'lineReview',
        name: 'lineReview',
        auth: 'lineReview',
        component: lineReview
      },
      {
        zhName: '线路审核详情',
        path: 'lineReviewDetail',
        name: 'lineReviewDetail',
        auth: 'lineReview',
        component: lineReviewDetail
      },
      {
        zhName: '线路评价',
        path: 'lineEvaluate',
        name: 'lineEvaluate',
        auth: 'lineEvaluate',
        component: lineEvaluate
      },
      {
        zhName: '线路评价详情',
        path: 'lineEvaluateDetail',
        name: 'lineEvaluateDetail',
        auth: 'lineEvaluate',
        component: lineEvaluateDetail
      },
      {
        zhName: '拼购商品',
        path: 'groupBuyingList',
        name: 'groupBuyingList',
        auth: 'groupBuyingList',
        component: groupBuyingList
      },
      {
        zhName: '拼购商品',
        path: 'groupBuyingAdd',
        name: 'groupBuyingAdd',
        auth: 'groupBuyingList',
        component: groupBuyingAdd
      },
      {
        zhName: '拼购审核',
        path: 'groupBuyingAudit',
        name: 'groupBuyingAudit',
        auth: 'groupBuyingAudit',
        component: groupBuyingAudit
      },
      {
        zhName: '优惠券',
        path: 'discountCouponList',
        name: 'discountCouponList',
        auth: 'discountCouponList',
        component: discountCouponList
      },
      {
        zhName: '优惠券详情',
        path: 'discountCouponDetail',
        name: 'discountCouponDetail',
        auth: 'discountCouponList',
        component: discountCouponDetail
      },
      {
        zhName: '会员套餐',
        path: 'memberComboList',
        name: 'memberComboList',
        auth: 'memberComboList',
        component: memberComboList
      },
      {
        zhName: '会员套餐配置',
        path: 'memberComboDetail',
        name: 'memberComboDetail',
        auth: 'memberComboList',
        component: memberComboDetail
      },
      {
        zhName: '会员信息',
        path: 'memberInfoList',
        name: 'memberInfoList',
        auth: 'memberInfoList',
        component: memberInfoList
      },
      {
        zhName: '会员信息详情',
        path: 'memberInfoDetail',
        name: 'memberInfoDetail',
        auth: 'memberInfoList',
        component: memberInfoDetail
      },
      {
        zhName: '开通记录',
        path: 'openRecordList',
        name: 'openRecordList',
        auth: 'openRecordList',
        component: openRecordList
      },
      {
        zhName: '会员协议',
        path: 'memberAgreementList',
        name: 'memberAgreementList',
        auth: 'memberAgreementList',
        component: memberAgreementList
      }, 
      {
        zhName: '订单列表',
        path: 'orderList',
        name: 'orderList',
        auth: 'orderList',
        component: orderList
      },
      {
        zhName: '订单详情',
        path: 'orderDetail',
        name: 'orderDetail',
        auth: 'orderList',
        component: orderDetail
      },
      {
        zhName: '加盟商列表',
        path: 'allianceList',
        name: 'allianceList',
        auth: 'allianceList',
        component: allianceList
      },
      {
        zhName: '加盟商详情',
        path: 'allianceDetail',
        name: 'allianceDetail',
        auth: 'allianceList',
        component: allianceDetail
      },
      {
        zhName: '加盟商账号信息',
        path: 'alliancAccountInfo',
        name: 'alliancAccountInfo',
        auth: 'allianceList',
        component: alliancAccountInfo
      },
      {
        zhName: '加盟商等级列表',
        path: 'allianceLevelList',
        name: 'allianceLevelList',
        auth: 'allianceLevelList',
        component: allianceLevelList
      }
    ]
  }
]