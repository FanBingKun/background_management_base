import isLogin from "./isLogin";
import clearAjax from "./clearAjax";

export default {
    isLogin,
    clearAjax
}