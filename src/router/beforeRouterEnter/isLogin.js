
import store from '@/store'
import router from '@/router'

export default (to, from, next) => {
    console.log('lai l ')
    // 判断有没有登陆过
    let userData = localStorage.getItem('userData')
    // 去登陆页面不做判断
    console.log(to)
    if (to.path === '/login' && userData) {
      next('/home')
    }else if(to.path === '/login') {
      next()
    } else {
      if (userData) {
        // 有登陆过的话 看有没获取过菜单
        if (!store.state.menu.hasMenu) {
          store.dispatch('menu/getMenu').then((res) => {
            let arr = res
            console.log(arr)
            router.addRoutes(arr)
            // 这边大坑  路径
            next(to.fullPath)
          })
        } else {
          next()
        }
      } else {
        next('/login')
      }
    }
  }